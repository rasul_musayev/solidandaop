package az.ingress.ms10gradlespring.services;

import javax.transaction.Transactional;

@Transactional
public class Service1 {
    public void sayHello0() {
        sayHello();//from inside the class
    }

    public void sayHello() {
        System.out.println("Hello World");
    }
}
