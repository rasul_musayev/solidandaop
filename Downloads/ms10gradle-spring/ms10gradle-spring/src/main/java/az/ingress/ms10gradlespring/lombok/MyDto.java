package az.ingress.ms10gradlespring.lombok;

import lombok.Data;

@Data
public class MyDto {
    private Integer id;
}
