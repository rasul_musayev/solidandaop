package az.ingress.ms10gradlespring.solid.dependencyInversion;

public class CreditCard implements BankCard {
    @Override
    public void doTransaction(int amount) {
        System.out.println("tx done with CreditCard");
    }
}
