package az.ingress.ms10gradlespring.services;

import az.ingress.ms10gradlespring.dto.Dto;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;
@Slf4j
@Service
public class HelloServiceImpl implements HelloService {
    @SneakyThrows
    public Dto sayHello(String language) {
        log.info("");
        Thread.sleep(new Random().nextInt(1000));
        return new Dto("Hello World");
    }


    @Override
    @SneakyThrows
    public Dto sayHello2() {
        Thread.sleep(new Random().nextInt(1000));

        return new Dto("Hello World");
    }

    @Override
    @SneakyThrows
    public Dto sayHello3() {
        Thread.sleep(new Random().nextInt(1000));

        return new Dto("Hello World");
    }
}
