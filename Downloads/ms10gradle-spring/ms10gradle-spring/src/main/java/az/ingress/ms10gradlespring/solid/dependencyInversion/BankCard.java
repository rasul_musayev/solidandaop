package az.ingress.ms10gradlespring.solid.dependencyInversion;

public interface BankCard {
       void  doTransaction(int amount);
}
