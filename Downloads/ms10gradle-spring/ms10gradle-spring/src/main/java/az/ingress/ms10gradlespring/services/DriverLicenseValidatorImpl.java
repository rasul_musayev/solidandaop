package az.ingress.ms10gradlespring.services;

import java.util.Date;

public class DriverLicenseValidatorImpl {

    public boolean isValid(DriverLicenseDto dto) {
        if (dto.getExpireDate().before(new Date())) {
            return false;
        } else if (dto.getCountry().equals("Russia")) {
            return false;
        }
        return true;

    }
}