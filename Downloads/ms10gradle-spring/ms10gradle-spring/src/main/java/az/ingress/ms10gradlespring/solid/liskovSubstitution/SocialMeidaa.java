package az.ingress.ms10gradlespring.solid.liskovSubstitution;

public interface SocialMeidaa {
     void chatWithFriend();
     void sendPhotosAndVideos();

}