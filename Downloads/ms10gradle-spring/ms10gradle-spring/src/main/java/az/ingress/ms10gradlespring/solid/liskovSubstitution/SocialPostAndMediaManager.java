package az.ingress.ms10gradlespring.solid.liskovSubstitution;

public interface SocialPostAndMediaManager {
     void publishPost(Object post);
}
