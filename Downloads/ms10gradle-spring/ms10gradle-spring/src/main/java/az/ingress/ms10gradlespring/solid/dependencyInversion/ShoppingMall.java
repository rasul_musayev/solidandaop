package az.ingress.ms10gradlespring.solid.dependencyInversion;

public class ShoppingMall {
    private DebitCard debitCard;
    private BankCard bankCard;

    public ShoppingMall(BankCard bankCard) {
        this.bankCard = bankCard;
    }

    public ShoppingMall(DebitCard debitCard){
        this.debitCard=debitCard;
    }
    public  void doPayment(Object order ,int amount){
        debitCard.doTransaction(amount);
    }


    public static void main(String[] args) {
        DebitCard debitCard = new DebitCard();
        ShoppingMall shoppingMall = new ShoppingMall(debitCard);
        shoppingMall.doPayment("some order ",50000);

//        BankCard bankCard = new CreditCard();
//        ShoppingMall shoppingMall1 = new ShoppingMall(bankCard);
//        shoppingMall1.doPayment("do some order",1000);

    }
}
