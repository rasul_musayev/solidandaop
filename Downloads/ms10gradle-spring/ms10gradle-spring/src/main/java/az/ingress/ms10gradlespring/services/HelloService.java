package az.ingress.ms10gradlespring.services;

import az.ingress.ms10gradlespring.dto.Dto;

public interface HelloService {
    Dto sayHello(String language);

    Dto sayHello2();

    Dto sayHello3();

}
