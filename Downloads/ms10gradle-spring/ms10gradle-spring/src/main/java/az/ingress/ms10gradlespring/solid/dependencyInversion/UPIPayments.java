package az.ingress.ms10gradlespring.solid.dependencyInversion;

public interface UPIPayments {
    void payMoney();
    void getScratchCard();
}
