package az.ingress.ms10gradlespring.solid.openClose;

public interface NotificationService {
     void sendOTP(String medium);
     void sendTransactionNotification(String medium);
}
