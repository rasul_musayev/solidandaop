package az.ingress.ms10gradlespring.solid.liskovSubstitution;

public interface VideoCallManager {
     void groupVideoCall(String... users);

}
