package az.ingress.ms10gradlespring.solid.singleton;

public class PasswordHasher {

    public String hashAndSavePassword(String password) {
        hashPassword();
        savePassword();
        return password;
    }

    public void hashPassword() {
    }

    public void savePassword() {
    }
}
