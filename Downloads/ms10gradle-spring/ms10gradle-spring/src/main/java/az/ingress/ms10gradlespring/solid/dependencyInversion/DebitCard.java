package az.ingress.ms10gradlespring.solid.dependencyInversion;

public class DebitCard implements BankCard {

    @Override
    public void doTransaction(int amount) {
        System.out.println("tx done with DebitCard");
    }
}
