package az.ingress.ms10gradlespring.services;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class ExecutionTimeLogger {
    @Pointcut(value = "execution(* az.ingress.ms10gradlespring.services.HelloServiceImpl.*(..))")
    private void logExecutionTimePc() {

    }

    @SneakyThrows
    @Around(value = "logExecutionTimePc()")
    public void logExecutionTime(ProceedingJoinPoint jp) {
        long started = System.currentTimeMillis();
        //method
        jp.proceed();
        long ended = System.currentTimeMillis();
        log.info("Elapsed time {}", ended - started);
    }

    // @SneakyThrows
//    @Before(value = "logExecutionTimePc()")
//    public  void logExecutionTime(Joinpoint joinpoint){
//        Arrays.stream(joinpoint.get)
//                .forEach(arg)->log.info("arguments are {}",arg);
//    }
}
